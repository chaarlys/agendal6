<?php

namespace App\Http\Controllers;

use App\proprietarios;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProprietariosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $proprietarios = proprietarios::all();
        return view('proprietarios.index', $proprietarios);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\proprietarios  $proprietarios
     * @return \Illuminate\Http\Response
     */
    public function show(proprietarios $proprietarios)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\proprietarios  $proprietarios
     * @return \Illuminate\Http\Response
     */
    public function edit(proprietarios $proprietarios)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\proprietarios  $proprietarios
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, proprietarios $proprietarios)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\proprietarios  $proprietarios
     * @return \Illuminate\Http\Response
     */
    public function destroy(proprietarios $proprietarios)
    {
        //
    }
}
