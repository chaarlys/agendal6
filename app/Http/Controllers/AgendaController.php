<?php

namespace App\Http\Controllers;

use App\Agenda;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class AgendaController extends Controller
{
/**
 * index - exibe uma lista dos registros da tabela
 * create - exibe um formulario para cadastrar dados
 * store - recebe os dados do formulario (create) e envia para o model
 *                                               gravar na tabela (opecação de inserçaõ)
 * edit - exibe um formulario para alterar dados de um determinado registro lido.
 * update - recebe os dados do formulario (edit) e envia para o Model atualizar 
 *                                               na tabela (operação de update)
 * show - exibe em detalhes os dados de um determinado registro
 * destroy - deleta um determinado registro
 */




    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contatos = Agenda::all();
        return view('agenda.index', compact('contatos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('agenda.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //$dt_nasc = $this->formata_data($request->dt_nasc);

        //dd($request);

        DB::table('agendas')->insert([

            'nome' => $request->nome,
            'fone_res' => $request->fone_res,
            'fone_cel' => $request->fone_cel,
            'dt_nasc' => $request->dt_nasc,
            'facebook' => $request->facebook,
            'twitter' => $request->twitter,
            'instagram' => $request->instagram,
            'email' => $request->email,

        ]);
   //     dd($x);

        return redirect()->route('agenda.index');

        //return view('agenda.index');
    }

    public function formata_data($data){

        $d = substr($data, 6, 4) . "-" . substr($data, 3, 2) . "-" . substr($data, 0, 2);

        return $d;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Agenda  $agenda
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $agenda = Agenda::find($id);

        return view('agenda.show', compact('agenda'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Agenda  $agenda
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $agenda = Agenda::find($id);

        return view('agenda.edit', compact('agenda'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Agenda  $agenda
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $dt_nasc = $this->formata_data($request->dt_nasc);

        //dd($request,$dt_nasc);

        DB::table('agendas')
        
        ->where('id',$id)
        ->update([

            'nome' => $request->nome,
            'fone_res' => $request->fone_res,
            'fone_cel' => $request->fone_cel,
            'dt_nasc' => $dt_nasc,
            'facebook' => $request->facebook,
            'twitter' => $request->twitter,
            'instagram' => $request->instagram,
            'email' => $request->email,

        ]);
   //     dd($x);

        return redirect()->route('agenda.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Agenda  $agenda
     * @return \Illuminate\Http\Response
     */
    public function destroy(Agenda $agenda)
    {
        //
    }
}
