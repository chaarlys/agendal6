<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Proprietarios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proprietarios', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nome',70);
            $table->string('sexo',1)->nullable();
            $table->date('dt_nasc')->nullable();
            $table->string('Naturalidade',50)->nullable();
            $table->string('Nacionalidade',50)->nullable();
            $table->string('Estado Civil',20)->nullable();
            $table->string('RG',9)->nullable();
            $table->string('Òrgão Emissor',10)->nullable();
            $table->string('profissão',50)->nullable();
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('Telefone Residencial',15);
            $table->string('Celular',20)->nullable(); 
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
    });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
