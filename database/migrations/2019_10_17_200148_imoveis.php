<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Imoveis extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('imoveis', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nome',70);
            $table->date('dt_nasc')->nullable();
            $table->string('Naturalidade',50)->nullable();
            $table->string('Estado Civil',20)->nullable();
            $table->string('RG',9)->nullable();
            $table->string('Òrgão Emissor',10)->nullable();
            $table->string('Telefone Residencial',15);
            $table->string('Celular',20)->nullable(); 
            $table->string('estado',50)->nullable();
            $table->string('cidade',50)->nullable();
            $table->string('bairro',50)->nullable();
            $table->string('n_dependente',20)->nullable();
            $table->float('renda',8,2)->nullable();


            $table->rememberToken();
            $table->timestamps();
    });
}

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
